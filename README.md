# Shaman (Julia edition)

Shaman is a library that use operator overloading and a novel method to evaluate the numerical accuracy of an application.
It has been designed to target high-performance computing and simulations.

This particular implementation is based on Julia, a programming language focused on numerical computing.
Due to its dynamic nature and to the care given to generic implementations, Julia is a perfect fit for Shaman as it makes the overload-based instrumentation of existing codebases easy.

## Installation

```
] add https://gitlab.com/numerical_shaman/shaman_julia
```

## Example

Shaman defines the types `SFloat16`, `SFloat32`, `SFloat64` and `SFloat128` that
can be used as regular floating point numbers but are aware of rounding errors.

When an instrumented number is printed, only its significant digits will be displayed (the symbol `~numerical-noise~` means that a number has no significant digits):

```
julia> x = [1.0, 1e10, -1e10]
3-element Array{Float64,1}:
  1.0
  1.0e10
 -1.0e10

julia> sum(Float64.(x))
1.0

julia> sum(Float32.(x))
0.0f0

julia> using Shaman
[ Info: Precompiling Shaman [dffee4b9-947b-4de9-9b0b-eb10db05c99f]

julia> sum(SFloat64.(x))
1.00000000000000000e+00

julia> sum(SFloat32.(x))
~numerical-noise~

julia> sum(SFloat32.(x)) |> corrected_value
1.0
```

Users also have access to a `reliable_digits` function to get the number of significant digits of a result and a `corrected_value` function to try and correct a result by taking its numerical error into account (note that once a result a corrected, there is no way to measure its accuracy).

## Contributing

We are happy to accept contributions and would be glad to help anyone wanting to contribute but unfamiliar with the underlying method.
The following are the highest priority elements we would like to add to this implementation:

- add missing mathematical functions to cover the full standard library (use a macro to generate wrapper for most mathematical function ?)
- add BLAS functions overloads (gemv!, etc)
- implement tagged error to localize sources of error (see C++ implementation and publication)

## References

The inner workings of Shaman are detailed in [Nestor Demeure's PhD](https://tel.archives-ouvertes.fr/tel-03116750).
You can reference it with:

```
@phdthesis{demeure_phd,
  TITLE = {{Compromise between precision and performance in high performance computing.}},
  AUTHOR = {Demeure, Nestor},
  URL = {https://tel.archives-ouvertes.fr/tel-03116750},
  SCHOOL = {{{\'E}cole Normale sup{\'e}rieure Paris-Saclay}},
  YEAR = {2021},
  MONTH = Jan,
  TYPE = {Theses}
}
```
