module Shaman

using Printf
using Random
using ErrorfreeArithmetic
import Format.format
import Quadmath.Float128

export SNum, SFloat16, SFloat32, SFloat64, SFloat128
export corrected_value, reliable_digits

#------------------------------------------------------------------------------
# Type definition

# encapsulate a number and its numerical error
struct SNum{T} <: AbstractFloat where T <: AbstractFloat
    value :: T
    error :: T
end

# predefines common numerical types
const SFloat16 = SNum{Float16}
higher_precision_type(::Type{Float16}) = Float32
const SFloat32 = SNum{Float32}
higher_precision_type(::Type{Float32}) = Float64
const SFloat64 = SNum{Float64}
higher_precision_type(::Type{Float64}) = Float128
const SFloat128 = SNum{Float128}
higher_precision_type(::Type{Float128}) = Base.BigFloat
# throws on type that have no defined higher precision type
higher_precision_type(::T) where T = throw("Please define 'higher_precision_type' for your type T such that it returns a type that can be used when higher precision is needed in numerical error computation.")

# constructors
SNum{T}(x::T) where T = SNum(x, zero(T))
SNum{T}(x::F) where {T,F<:Number} = SNum(T(x), T( higher_precision_type(T)(x) - T(x) ))
SNum{T}(x::SNum{T}) where T = SNum(x.value, x.error)
SNum{T}(x::SNum{F}) where {T,F} = SNum(T(x.value), T( (higher_precision_type(T)(x.value) - T(x.value)) + x.error ) )

# casts
Base.Bool(x::SNum{T}) where T = Base.Bool(x.value)
Base.Int8(x::SNum{T}) where T = Base.Int8(x.value)
Base.Int16(x::SNum{T}) where T = Base.Int16(x.value)
Base.Int32(x::SNum{T}) where T = Base.Int32(x.value)
Base.Int64(x::SNum{T}) where T = Base.Int64(x.value)
Base.Int128(x::SNum{T}) where T = Base.Int128(x.value)
Base.UInt8(x::SNum{T}) where T = Base.UInt8(x.value)
Base.UInt16(x::SNum{T}) where T = Base.UInt16(x.value)
Base.UInt32(x::SNum{T}) where T = Base.UInt32(x.value)
Base.UInt64(x::SNum{T}) where T = Base.UInt64(x.value)
Base.UInt128(x::SNum{T}) where T = Base.UInt128(x.value)
# set to false by default to avoid code invisibly casting back to Float64
allow_float_convertion = false
throw_on_convert(x) = if (allow_float_convertion) x else throw("Conversions back to non-instrumented float types are not allowed.") end
Base.Float64(x::SNum{T}) where T = Base.Float64(x.value) |> throw_on_convert
Base.Float32(x::SNum{T}) where T = Base.Float32(x.value) |> throw_on_convert
Base.Float16(x::SNum{T}) where T = Base.Float16(x.value) |> throw_on_convert
Float128(x::SNum{T}) where T = Float128(x.value) |> throw_on_convert
Base.MPFR.BigFloat(x::SNum{T}) where T = Base.MPFR.BigFloat(x.value) |> throw_on_convert

# promotion rule for mixed precision operations
base_type(::Type{T}) where T<:Number = T
base_type(::Type{SNum{T}}) where T = T
Base.promote_rule(::Type{T1}, ::Type{T2}) where {T1<:SNum, T2} = SNum{promote_type(base_type(T1), base_type(T2))}
Base.promote_rule(::Type{T1}, ::Type{T2}) where {T1<:AbstractIrrational, T2<:SNum} = SNum{promote_type(base_type(T1), base_type(T2))}

#------------------------------------------------------------------------------
# Methods

# tries to return a better approximation of the number by taking the numerical error into account
function corrected_value(x::SNum{T}) where T<:AbstractFloat
    higher_precision_type(T)(x.value) + higher_precision_type(T)(x.error)
end

# returns the number of (relative) significative digits of a couple (number,error)
# the exact 0 is handled by a special case (otherwise it cannot have any significativ digits)
function reliable_digits(value::T, error::T) where T
    if iszero(error)
        # no error -> theorically infinite precision
        typemax(T)
    elseif isnan(error)
        # if the error reached nan, we can trust no digit
        zero(T)
    elseif iszero(value)
        # we count the number of significant zeroes
        max(zero(T), -log10(abs(error)) - one(T))
    else
        relativeError = abs(error / value)
        if relativeError >= one(T)
            zero(T)
        else
            -log10(relativeError)
        end
    end
end

# returns the number of (relative) significative digits of an SNum
function reliable_digits(x::SNum)
    reliable_digits(x.value, x.error)
end

# overloads for compatibility with the random number generator
Random.rand(rng::AbstractRNG, ::SNum{T}) where T = SNum{T}( Random.rand(rng, Random.Sampler(rng, T, Val(1))) )
Random.rand(rng::AbstractRNG, ::Type{SNum{T}}) where T = SNum{T}( Random.rand(rng, Random.Sampler(rng, T, Val(1))) )
Random.rand(rng::AbstractRNG, ::SNum{T}, dims::Dims) where T = SNum{T}.( Random.rand!(rng, Array{Random.gentype(T)}(undef, dims), T) )
Random.rand(rng::AbstractRNG, ::Type{SNum{T}}, dims::Dims) where T = SNum{T}.( Random.rand!(rng, Array{T}(undef, dims), T) )

#------------------------------------------------------------------------------
# Printing

# string conversion operator
# displays only the significant digits
function Base.string(x::SNum{T}) where T <: AbstractFloat
    nbDigitsMax = 17 # maximum number of digits displayed
    fdigits = x |> reliable_digits |> floor
    if isnan(x.value) # not a number
        "Nan"
    elseif isnan(x.error)
        "~Nan~"
    elseif fdigits <= zero(T) # no significant digits
        # the first zeros might be significant
        digits = reliable_digits(zero(T), x.error) |> floor |> Integer
        if (abs(x.value) >= one(T)) || (digits <= zero(T)) # the number has no meaning
            "~numerical-noise~"
        else # some zeros are significant
            digits = min(nbDigitsMax, digits)
            format(zero(T), precision=digits, conversion="e")
        end
    else # a perfectly fine number
        digits = min(nbDigitsMax, fdigits) |> Int64 # not Int8 since Float128 only converts to Int64
        format(x.value, precision=digits, conversion="e")
    end
end

# display function
Base.show(io::IO, ::Type{SNum{T}}) where T = print(io, string("S", T))
Base.show(io::IO, x::SNum) = print(io, Base.string(x))

# raw display function
Base.repr(x::SNum) = string(repr(typeof(x)), "{val:", repr(x.value), " err:", repr(x.error), "}")

# avoid infinite loop in printf definition
Printf.ini_dec(x::SNum, n::Int, digits) = Printf.ini_dec(x.value,n,digits)
Printf.fix_dec(x::SNum, n::Int, digits) = Printf.fix_dec(x.value,n,digits)

#------------------------------------------------------------------------------
# operators

function Base.:-(x::SNum{T}) where T
    value = -x.value
    error = -x.error
    SNum(value, error)
end

function Base.:+(x::SNum{T}, y::SNum{T}) where T
    value, op_error = ErrorfreeArithmetic.two_sum(x.value, y.value)
    error = x.error + y.error + op_error
    SNum(value, error)
end

function Base.:-(x::SNum{T}, y::SNum{T}) where T
    #value, op_error = ErrorfreeArithmetic.two_diff(x.value, y.value)
    value, op_error = ErrorfreeArithmetic.two_sum(x.value, -y.value)
    error = x.error - y.error + op_error
    SNum(value, error)
end

function Base.:*(x::SNum{T}, y::SNum{T}) where T
    value, op_error = ErrorfreeArithmetic.two_prod(x.value, y.value)
    error = x.error*y.value + x.value*y.error + op_error
    SNum(value, error)
end

function Base.:/(x::SNum{T}, y::SNum{T}) where T
    value, op_remainder = ErrorfreeArithmetic.div_rem(x.value, y.value)
    error = ((op_remainder + x.error) - value*y.error) / (y.value + y.error)
    SNum(value, error)
end

# binary comparison operators
for op = (:cmp, :(==), :(!=), :(>=), :(<=), :(>), :(<), :isless, :isequal)
    eval(quote
        Base.$op(x::SNum, y::SNum) = $op(x.value, y.value)
        Base.$op(x::SNum, y::Number) = $op(x.value, y)
        Base.$op(x::Number, y::SNum) = $op(x, y.value)
        Base.$op(x::SNum, y::Real) = $op(x.value, y)
        Base.$op(x::Real, y::SNum) = $op(x, y.value)
    end)
end

# unary comparison operators
for op = (:iszero, :isone, :isnan, :isinf, :isinteger)
    eval(quote
        Base.$op(x::SNum) = $op(x.value)
    end)
end

#------------------------------------------------------------------------------
# functions

# type methods
for op = (:zero, :one, :typemin, :typemax, :floatmin, :floatmax, :eps, :precision)
    eval(quote
        Base.$op(::SNum{T}) where T = SNum{T}($op(T))
        Base.$op(::Type{SNum{T}}) where T = SNum{T}($op(T))
    end)
end

# simple unary functions
for op = (:exp, :cos, :sin, :tan, :atan, :cosh, :sinh, :atanh, :asinh, :exp2, :expm1, :cbrt, :ceil, :floor, :trunc, :round)
    eval(quote
        function Base.$op(x::SNum{T}) where T<:AbstractFloat
            value = $op(x.value)
            value_corr = x |> corrected_value |> $op
            error = T(value_corr - value)
            SNum(value, error)
        end
    end)
end

# various logarithms
for op = (:log, :log10, :log2)
    eval(quote
        function Base.$op(x::SNum{T}) where T<:AbstractFloat
            value = $op(x.value)
            value_corr = corrected_value(x)
            if (value_corr <= zero(higher_precision_type(T))) && (x.value <= zero(T))
                # value is, and should be, -inf or nan
                error = zero(T)
                SNum(value, error)
            elseif value_corr < zero(higher_precision_type(T))
                # only the value corrected is below zero
                error = typemin(T) #-infinity
                SNum(value, error)
            else
                # all is well
                error = T($op(value_corr) - value)
                SNum(value, error)
            end
        end
    end)
end

function Base.:abs(x::SNum{T}) where T <: AbstractFloat
    if x.value >= zero(T)
        x
    else
        -x
    end
end

Base.min(x::SNum, y::SNum) = x.value < y.value ? x : y
Base.max(x::SNum, y::SNum) = x.value > y.value ? x : y

function Base.:sqrt(x::SNum{T}) where T<:AbstractFloat
    value, op_remainder = ErrorfreeArithmetic.sqrt_rem(x.value)
    if iszero(value)
        error = x.error |> abs |> sqrt
        SNum(value, error)
    else
        error = (op_remainder + x.error) / (value + value)
        SNum(value, error)
    end
end

for op = (:atan, :div, :rem)
    eval(quote
             function Base.$op(x::SNum{T}, y::SNum{T}) where T
                 value = $op(x.value, y.value)
                 value_corr = $op(corrected_value(x), corrected_value(y))
                 error = T(value_corr - value)
                 SNum(value, error)
             end
         end)
end

end # module
