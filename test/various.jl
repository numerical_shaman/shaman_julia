using Shaman

# proposed by Siegfried Rump (rump's royal pain)
# http://www.davidhbailey.com/dhbtalks/dhb-num-repro.pdf
#
# the result is astronomicaly wrong !
# but consistant across precisions
function rumpTest(T)
    println("Rump test (", T, ")")
    x::T = 77617.0
    y::T = 33096.0
    x2 = x*x
    y2 = y*y
    y4 = y2*y2
    y6 = y4*y2
    y8 = y4*y4
    result = 333.75*y6 + x2*(11.0*x2*y2 - y6 - 121.0*y4 - 2.0) + 5.5*y8 + x/(2.0*y)
    println("result: ", result)
    println("true result: -0.82739605994682136814116")
end
rumpTest(Float64)
rumpTest(SFloat32) # test mixed precision
rumpTest(SFloat64)

# from Kahan, On the Cost of Floating-Point Computation Without Extra-Precise Arithmetic
function polynomialTest(T)
    println("Polynomial test (", T, ")")
    # a*x² + b*x + c = 0
    a::T = 94906265.625
    b::T = -189812534.0
    c::T = 94906268.375
    delta = b*b - 4.0*a*c;
    #r1 = (-b + sqrt(delta)) / (2*a);
    r2 = (-b - sqrt(delta)) / (2*a);
    #println("root1: ", r1, " (real value: 1.000000028975958)")
    println("root2: ", r2, " (real value: 1)")
end
polynomialTest(Float64)
polynomialTest(SFloat64)

# a polynomial proposed by RUMP
#
# the first result is bad, the second is perfect
# can we differentiate them ?
#
# One can see the difference of accuracy between the two results even if they were provided with the same algorithm.
# It points out the importance of the data influence on the numerical quality of results given by an algorithm.
#
# http://www-pequan.lip6.fr/cadna/Examples_Dir/ex1.php
function polynomial(T)
  println("Rump equation (", T, ")")
  P(x,y) = 9.0*x*x*x*x - y*y*y*y + 2.0*y*y
  # imprecise result
  x::T = 10864.0
  y::T = 18817.0
  res = P(x,y)
  println("P(10864,18817) = ", res, " (exact value : 1)")
  # very precise result
  x = 1.0 / 3.0
  y = 2.0 / 3.0
  res = P(x,y)
  println("P(1/3,2/3) = ", res, " (exact value : 0.8024691358024691)")
end
polynomial(Float64)
polynomial(SFloat64)
