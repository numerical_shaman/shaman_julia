using Shaman
using Test

x = SFloat64(1.0)
y = SFloat64(2.0)
@test min(x, y) == x
@test max(x, y) == y

@test atan(x, x) ≈ π/4

# Div and Rem do not work for SFloat64 because of they are not implemented for Float128
x = SFloat32(1.0)
y = SFloat32(2.0)
@test div(x, y) == 0f0
@test rem(x, y) == 1f0

x = SFloat64(1.0)
@test 0.5 < x < 2.0
@test x < π

x = SFloat32(1.0)
@test typeof((x + 1).value) == typeof(x.value + 1)
@test typeof((x + π).value) == typeof(x.value + π)
